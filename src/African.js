import { Parrot } from "./parrot";

export class African extends Parrot {
    
    getSpeed() {
        return Math.max(0, this.baseSpeed - this.loadFactor * this.numberOfCoconuts);
    }
}
